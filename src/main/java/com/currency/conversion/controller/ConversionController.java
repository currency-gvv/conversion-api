package com.currency.conversion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RestController
public class ConversionController {

    @Autowired
    private ExchangeApiProxy proxy;

    @GetMapping(path = "/conversion/from/{from}/to/{to}/quantity/{quantity}")
    public CurrencyConversionBean converCurrency(@PathVariable String from, @PathVariable String to, @PathVariable BigDecimal quantity){

        Map<String,String> pathVariables = new HashMap<>();
        pathVariables.put("from", from);
        pathVariables.put("to", to);

        ResponseEntity<CurrencyConversionBean> re = new RestTemplate().getForEntity("http://localhost:8000/exchange/from/{from}/to/{to}",
                CurrencyConversionBean.class,
                pathVariables);

        CurrencyConversionBean ccb = re.getBody();

        return new CurrencyConversionBean(1L, from, to, ccb.getConversionMultiple(), quantity, quantity.multiply(ccb.getConversionMultiple()),ccb.getPort());
    }

    @GetMapping(path = "/conversion-feign/from/{from}/to/{to}/quantity/{quantity}")
    public CurrencyConversionBean converCurrencyFeign(@PathVariable String from, @PathVariable String to, @PathVariable BigDecimal quantity){

        CurrencyConversionBean response = proxy.retrieveExchangeValue(from, to);

        return new CurrencyConversionBean(
                1L,
                from,
                to,
                response.getConversionMultiple(),
                quantity,
                quantity.multiply(response.getConversionMultiple()),response.getPort());
    }
}
