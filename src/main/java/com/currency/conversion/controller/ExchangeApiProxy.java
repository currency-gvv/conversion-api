package com.currency.conversion.controller;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient(name="exchange-api", url="localhost:8000")
@FeignClient(name="exchange-api")
@RibbonClient(name="exchange-api")
public interface ExchangeApiProxy {


    @GetMapping(path="/exchange/from/{from}/to/{to}")
    public CurrencyConversionBean retrieveExchangeValue(@PathVariable(value="from") String from, @PathVariable(value="to") String to);
}
